This repository contains the Archive Index Database in SQLite format compatible with 
libcbm.

The EU Archive Index Database was published at:

European Commission, Joint Research Centre, ‘The EU Archive Index Database customised 
for the Carbon Budget Model (CBM-CFS3)’, 2017 (updated 2017-05-05), accessed 2022-08-04, 
http://data.europa.eu/89h/jrc-cbm-eu-aidb

